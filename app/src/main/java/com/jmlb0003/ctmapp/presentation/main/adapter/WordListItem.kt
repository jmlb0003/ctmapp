package com.jmlb0003.ctmapp.presentation.main.adapter

import com.jmlb0003.ctmapp.domain.model.Word

sealed class WordListItem {

    object Header : WordListItem()

    class WordItem(val word: Word, val isPrime: Boolean) : WordListItem()

}
