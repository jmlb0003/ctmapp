package com.jmlb0003.ctmapp.presentation.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.jmlb0003.ctmapp.R
import kotlinx.android.synthetic.main.item_list_word.view.word_count
import kotlinx.android.synthetic.main.item_list_word.view.word_count_is_prime
import kotlinx.android.synthetic.main.item_list_word.view.word_label

sealed class WordListItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(item: WordListItem)

    class HeaderViewHolder(view: View) : WordListItemViewHolder(view) {

        override fun bind(item: WordListItem) {
            // Nothing to do for HeaderViewHolder
        }
    }

    class WordListViewHolder(view: View) : WordListItemViewHolder(view) {

        override fun bind(item: WordListItem) {
            val wordItem = item as WordListItem.WordItem
            with(itemView) {
                word_label.text = wordItem.word.word
                word_count.text =
                    resources.getQuantityString(R.plurals.words_counter, wordItem.word.count, wordItem.word.count)
                word_count_is_prime.text = if (wordItem.isPrime) {
                    resources.getString(R.string.is_prime_string)
                } else {
                    resources.getString(R.string.is_not_prime_string)
                }
            }
        }
    }

}
