package com.jmlb0003.ctmapp.presentation.main

import com.jmlb0003.ctmapp.app.utils.TasksSchedulers
import com.jmlb0003.ctmapp.data.utils.PrimeNumberHelper
import com.jmlb0003.ctmapp.domain.model.Word
import com.jmlb0003.ctmapp.domain.repository.BookRepository
import com.jmlb0003.ctmapp.presentation.base.BasePresenter
import com.jmlb0003.ctmapp.presentation.base.BasePresenterImp
import com.jmlb0003.ctmapp.presentation.main.adapter.WordListItem
import io.reactivex.Single
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

class MainActivityPresenter
@Inject constructor(
    private val schedulers: TasksSchedulers,
    private val repository: BookRepository
) : BasePresenterImp<MainActivityPresenter.MainActivityView>() {

    fun onViewInitialized() {
        getView()?.checkPermissions()
    }

    fun onPermissionsNotGranted() {
        getView()?.requestPermissions()
    }

    fun onPermissionsGranted() {
        disposables += repository.getBookWords()
            .normalizeDataToWordListItems()
            .subscribeOn(schedulers.getBackgroundThread())
            .observeOn(schedulers.getUiThread())
            .doOnSubscribe { getView()?.showLoading() }
            .doAfterTerminate { getView()?.hideLoading() }
            .subscribe(::doSomethingOnSuccess, ::doSomethingOnError)
    }

    private fun Single<List<Word>>.normalizeDataToWordListItems(): Single<List<WordListItem>> {
        return map { words: List<Word> ->
            listOf(WordListItem.Header) +
                words.sortedBy { it.word }.map { word ->
                    WordListItem.WordItem(word, PrimeNumberHelper.isPrime(word.count))
                }
        }
    }

    private fun doSomethingOnSuccess(wordList: List<WordListItem>) {
        getView()?.showWordList(wordList)
    }

    private fun doSomethingOnError(error: Throwable) {
        getView()?.showError(error.localizedMessage)
    }

    interface MainActivityView : BasePresenter.BaseView {

        fun showLoading()

        fun hideLoading()

        fun checkPermissions()

        fun requestPermissions()

        fun showWordList(wordList: List<WordListItem>)

        fun showError(error: String)

    }

}
