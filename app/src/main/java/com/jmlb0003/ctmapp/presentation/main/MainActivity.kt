package com.jmlb0003.ctmapp.presentation.main

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import com.jmlb0003.ctmapp.R
import com.jmlb0003.ctmapp.data.utils.WordCounter
import com.jmlb0003.ctmapp.presentation.base.BaseActivity
import com.jmlb0003.ctmapp.presentation.main.adapter.WordListItem
import com.jmlb0003.ctmapp.presentation.main.adapter.WordsAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.loading_view
import kotlinx.android.synthetic.main.activity_main.word_list_recycler_view
import javax.inject.Inject

private const val WRITE_PERMISSION_REQUEST_CODE = 123

class MainActivity : BaseActivity<MainActivityPresenter.MainActivityView, MainActivityPresenter>(),
    MainActivityPresenter.MainActivityView {

    @Inject
    lateinit var presenterInstance: MainActivityPresenter

    // region BaseActivity functions
    override fun getPresenter(): MainActivityPresenter = presenterInstance

    override fun getMVPViewReference(): MainActivityPresenter.MainActivityView = this
    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupWordList()
        presenterInstance.onViewInitialized()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            WRITE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenterInstance.onPermissionsGranted()
                } else {
                    presenterInstance.onPermissionsNotGranted()
                }
                return
            }
        }
    }

    // region MainActivityView functions
    override fun showLoading() {
        loading_view.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading_view.visibility = View.GONE
    }

    override fun checkPermissions() {
        if (!isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            presenterInstance.onPermissionsNotGranted()
        } else {
            presenterInstance.onPermissionsGranted()
        }
    }

    override fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            Array(1) { Manifest.permission.WRITE_EXTERNAL_STORAGE },
            WRITE_PERMISSION_REQUEST_CODE
        )
    }

    override fun showWordList(wordList: List<WordListItem>) {
        (word_list_recycler_view.adapter as WordsAdapter).setWordList(wordList)
    }

    override fun showError(error: String) {
        Toast.makeText(this, "There was an error: $error", Toast.LENGTH_LONG).show()
    }
    // endregion

    private fun setupWordList() {
        with(word_list_recycler_view) {
            adapter = WordsAdapter()
        }
    }

    private fun isPermissionGranted(permission: String) =
        ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

}
