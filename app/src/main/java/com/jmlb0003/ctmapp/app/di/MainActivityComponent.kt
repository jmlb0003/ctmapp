package com.jmlb0003.ctmapp.app.di

import com.jmlb0003.ctmapp.presentation.main.MainActivity
import dagger.Subcomponent

@Subcomponent(
    modules = [
        MainActivityModule::class
    ]
)
interface MainActivityComponent {
    fun inject(activity: MainActivity)
}
