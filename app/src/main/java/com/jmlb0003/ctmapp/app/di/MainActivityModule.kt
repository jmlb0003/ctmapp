package com.jmlb0003.ctmapp.app.di

import com.jmlb0003.ctmapp.app.utils.Schedulers
import com.jmlb0003.ctmapp.domain.repository.BookRepository
import com.jmlb0003.ctmapp.presentation.main.MainActivityPresenter
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideMainActivityPresenter(
        schedulers: Schedulers,
        repository: BookRepository
    ): MainActivityPresenter = MainActivityPresenter(schedulers, repository)
}
