package com.jmlb0003.ctmapp.app.utils

import io.reactivex.Scheduler

interface TasksSchedulers {

    fun getBackgroundThread(): Scheduler

    fun getUiThread(): Scheduler

}
