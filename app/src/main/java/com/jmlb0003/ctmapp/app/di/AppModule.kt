package com.jmlb0003.ctmapp.app.di

import com.jmlb0003.ctmapp.app.utils.Schedulers
import com.jmlb0003.ctmapp.app.utils.TasksSchedulers
import com.jmlb0003.ctmapp.data.utils.WordCounter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideRxSchedulers(): TasksSchedulers = Schedulers()

    @Provides
    @Singleton
    fun provideWordCounter(): WordCounter = WordCounter()

}
