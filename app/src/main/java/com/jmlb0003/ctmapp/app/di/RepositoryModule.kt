package com.jmlb0003.ctmapp.app.di

import com.jmlb0003.ctmapp.data.BookRepositoryImpl
import com.jmlb0003.ctmapp.data.network.NetworkDataSourceImpl
import com.jmlb0003.ctmapp.domain.repository.BookRepository
import com.jmlb0003.ctmapp.domain.repository.NetworkDataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideNetworkDataSource(networkDataSource: NetworkDataSourceImpl): NetworkDataSource

    @Binds
    @Singleton
    abstract fun provideRepository(repositoryImpl: BookRepositoryImpl): BookRepository

}
