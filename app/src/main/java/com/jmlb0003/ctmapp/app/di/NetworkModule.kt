package com.jmlb0003.ctmapp.app.di

import com.jmlb0003.ctmapp.app.utils.ServiceGenerator
import com.jmlb0003.ctmapp.data.network.FileDownloadService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideDownloadService(): FileDownloadService =
        ServiceGenerator.createService(FileDownloadService::class.java)

}
