package com.jmlb0003.ctmapp.data.network

import android.os.Environment
import com.jmlb0003.ctmapp.domain.repository.NetworkDataSource
import io.reactivex.Single
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import javax.inject.Inject

class NetworkDataSourceImpl
@Inject constructor(
    private val downloadService: FileDownloadService
) : NetworkDataSource {

    override fun getBook(): Single<String> {
        return Single.create<String> { emitter ->
            try {
                downloadService.downloadBook().execute().body()?.let { response ->

                    val path =
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    val file = File(path, "book.txt")

                    response.byteStream().use { inputStream ->
                        inputStream.writeInFile(file)
                        inputStream.close()
                    }

                    emitter.onSuccess(file.absolutePath)
                }
            } catch (e: IOException) {
                emitter.onError(Throwable(e.message))
            }
            if (!emitter.isDisposed) {
                emitter.onError(Throwable("We had an issue downloading the book"))
            }
        }
    }

    private fun InputStream.writeInFile(file: File) {
        FileOutputStream(file).use { fileOutput ->
            val buffer = ByteArray(4096)
            var read = read(buffer)

            while (read != -1) {
                fileOutput.write(buffer, 0, read)
                read = read(buffer)
            }

            fileOutput.flush()
            fileOutput.close()
        }
    }

}
