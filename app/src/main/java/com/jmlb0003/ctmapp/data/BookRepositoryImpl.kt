package com.jmlb0003.ctmapp.data

import com.jmlb0003.ctmapp.data.utils.WordCounter
import com.jmlb0003.ctmapp.domain.repository.BookRepository
import com.jmlb0003.ctmapp.domain.repository.NetworkDataSource
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import javax.inject.Inject

class BookRepositoryImpl
@Inject constructor(
    private val networkDataSource: NetworkDataSource,
    private val wordCounter: WordCounter
) : BookRepository {

    override fun getBookWords() =
        networkDataSource.getBook()
            .map { filePath ->
                val file = File(filePath)
                if (file.exists()) {
                    val input = FileInputStream(file)
                    val reader = BufferedReader(InputStreamReader(input))
                    var line = reader.readLine()
                    while (line != null) {
                        wordCounter.addText(line)
                        line = reader.readLine()
                    }

                    reader.close()
                    input.close()
                }
                wordCounter.getWordList()
            }

}
