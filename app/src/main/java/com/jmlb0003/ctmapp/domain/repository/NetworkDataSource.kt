package com.jmlb0003.ctmapp.domain.repository

import io.reactivex.Single

interface NetworkDataSource {

    fun getBook(): Single<String>

}
