package com.jmlb0003.ctmapp.domain.model

data class Word(
    val word: String,
    var count: Int = 1
)
