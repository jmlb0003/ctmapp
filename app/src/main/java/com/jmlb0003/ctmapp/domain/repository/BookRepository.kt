package com.jmlb0003.ctmapp.domain.repository

import com.jmlb0003.ctmapp.domain.model.Word
import io.reactivex.Single

interface BookRepository {

    fun getBookWords(): Single<List<Word>>

}
