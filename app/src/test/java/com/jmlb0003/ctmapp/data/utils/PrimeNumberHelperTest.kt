package com.jmlb0003.ctmapp.data.utils

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class PrimeNumberHelperTest {

    private val helper = PrimeNumberHelper

    @Test
    fun `on isPrime returns false if given number is less than 2`() {
        assertFalse(helper.isPrime(0))
        assertFalse(helper.isPrime(1))
    }

    @Test
    fun `on isPrime returns true if given number is prime and greater than 1`() {
        assertTrue(helper.isPrime(2))
        assertTrue(helper.isPrime(3))
        assertTrue(helper.isPrime(5))
        assertTrue(helper.isPrime(191))
    }

    @Test
    fun `on isPrime returns false if given number is not prime and greater than 1`() {
        assertFalse(helper.isPrime(4))
        assertFalse(helper.isPrime(6))
        assertFalse(helper.isPrime(8))
        assertFalse(helper.isPrime(160))
        assertFalse(helper.isPrime(333))
    }

}