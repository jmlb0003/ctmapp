package com.jmlb0003.ctmapp.data.utils

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class WordCounterTest {

    private val textWithWordsRepeated = "Text with words repeated in the text  . "

    private val wordCounter = WordCounter()

    @Test
    fun `on getWordList returns empty list when no words were added`() {
        assertTrue(wordCounter.getWordList().isEmpty())
    }

    @Test
    fun `on getWordList returns empty list when a blank word was added`() {
        wordCounter.addText(" ")
        assertTrue(wordCounter.getWordList().isEmpty())
    }

    @Test
    fun `on getWordList returns the list with one word and count 1 when one valid word given`() {
        val word = "something"

        wordCounter.addText(word)

        assertTrue(wordCounter.getWordList().isNotEmpty())
        assertEquals(word, wordCounter.getWordList()[0].word)
        assertEquals(1, wordCounter.getWordList()[0].count)
    }

    @Test
    fun `on getWordList returns the list with one word and count 2 when one valid word given twice`() {
        val word = "something"

        wordCounter.addText(word)
        wordCounter.addText(word)

        assertEquals(1, wordCounter.getWordList().size)
        assertEquals(word, wordCounter.getWordList()[0].word)
        assertEquals(2, wordCounter.getWordList()[0].count)
    }

    @Test
    fun `on getWordList returns the list of words with times appearing in a given text`() {
        wordCounter.addText(textWithWordsRepeated)

        assertEquals(6, wordCounter.getWordList().size)
        assertEquals("text", wordCounter.getWordList()[0].word)
        assertEquals(2, wordCounter.getWordList()[0].count)
        assertEquals("with", wordCounter.getWordList()[1].word)
        assertEquals(1, wordCounter.getWordList()[1].count)
        assertEquals("words", wordCounter.getWordList()[2].word)
        assertEquals(1, wordCounter.getWordList()[2].count)
        assertEquals("repeated", wordCounter.getWordList()[3].word)
        assertEquals(1, wordCounter.getWordList()[3].count)
        assertEquals("in", wordCounter.getWordList()[4].word)
        assertEquals(1, wordCounter.getWordList()[4].count)
        assertEquals("the", wordCounter.getWordList()[5].word)
        assertEquals(1, wordCounter.getWordList()[5].count)
    }

}
